# O
  - Code Review : find myself still not making testing a habit, the last commit because focused on the code and forgot to run the test，therefore don't know a bug has occurred, should be kept in mind as a lesson.
  - TDD、OOP、Refactor Review ： It has consolidated the knowledge learned before, and corrected some previous wrong understanding. Give myself a deeper understanding of the knowledge I have learned before.
  - Pair Programming : Pair programming makes me have a different idea in the development process, which is conducive to the development of code, and at the same time, find out the error in the coding process and correct it in time.
  - Spring Boot：I learned the basic usage of Spring Boot, including the construction of projects and the use of some annotations and syntax, and was able to set up a basic server.
# R
  -  Helpful!
# I
  - I think today's let me learned the new technology of Spring Boot. At the same time experience the charm of pair programming.It also reminded me of what I had learned before.
# D
  - Get in the habit of running tests after every feature change.
  - Trained myself to program collaboratively with others through pair programming.
  - Learn more about Spring Boot and practice it.