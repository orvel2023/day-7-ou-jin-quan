package repository;

import com.thoughtworks.springbootemployee.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

public class EmployeeRepository {
    private static final List<Employee> employees = new ArrayList<>();
    private static final AtomicLong atomicId = new AtomicLong(0);
    public List<Employee> getAll() {
        return employees;
    }
    public Employee insert(Employee employee) {
        employee.setId(atomicId.incrementAndGet());
        employees.add(employee);
        return employee;
    }
    public Employee getById(Long id) {
        return employees.stream().filter(employee -> employee.getId().equals(id)).findFirst().get();
    }
    public List<Employee> getByGender(String gender) {
        return employees.stream().filter(employee -> employee.getGender().equals(gender)).collect(Collectors.toList());
    }

    public void deleteById(Long id) {
        employees.remove(employees.stream().filter(employee -> employee.getId().equals(id)).findFirst().get());
    }

    public Employee updateById(Long id, Employee employee) {
        Employee employeeTemp = employees.stream().filter(employeeInList -> employeeInList.getId().equals(id)).findFirst().get();
        employeeTemp.setAge(employee.getAge());
        employeeTemp.setSalary(employee.getSalary());
        return employeeTemp;
    }

    public List<Employee> getByPageAndSize(Integer page, Integer size) {

        return employees.stream().skip((long) size * (page - 1)).limit(size).collect(Collectors.toList());
    }

    public List<Employee> getByCompanyId(Long companyId) {
        return employees.stream().filter(employee -> employee.getCompanyId().equals(companyId)).collect(Collectors.toList());
    }
}
