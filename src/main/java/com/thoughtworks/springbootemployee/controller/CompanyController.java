package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import repository.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    EmployeeRepository employeeRepository = new EmployeeRepository();
    private final AtomicLong atomicId = new AtomicLong(0);
    private final List<Company> companyList = new ArrayList<>();

    @PostMapping
    public ResponseEntity<Company> createCompany(@RequestBody Company company) {
        company.setId(atomicId.incrementAndGet());
        companyList.add(company);
        return ResponseEntity.status(HttpStatus.CREATED).body(company);
    }

    @GetMapping
    public List<Company> getCompanyList() {
        return companyList;
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companyList.stream().filter(company -> company.getId().equals(id)).findFirst().get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompanyById(@PathVariable Long id) {
       companyList.removeIf(company -> company.getId().equals(id));
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesByPageAndSize(@RequestParam Integer page, @RequestParam Integer size) {
        return companyList.stream().skip((long) size * (page - 1)).limit(size).collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    public Company updateCompanyById(@PathVariable Long id, @RequestBody Company company) {
        Company companyTemp = companyList.stream().filter(companyInList -> companyInList.getId().equals(id)).findFirst().get();
        companyTemp.setName(company.getName());
        return companyTemp;
    }
    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id){
        return employeeRepository.getByCompanyId(id);
    }
}
